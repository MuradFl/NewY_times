import 'package:dio/dio.dart';
import 'package:ny_times/models/article_model.dart';

class HomeRepository {
  final Dio _dio =
      Dio(BaseOptions(baseUrl: 'https://api.nytimes.com/svc/topstories/v2'));

  Future<List<Results>> fetch(String section) async {
    final response = await _dio.get('/$section.json',
        queryParameters: {'api-key': 'G4b8jcxtWmKgzTqOm86fPbqeLNE7Cvbp'});

    final List<Results> items = [];
    response.data['results'].forEach((json) {
      items.add(Results.fromJson(json));
    });
    return items;
  }
}
