import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:ny_times/controllers/main_controller.dart';
import 'package:ny_times/models/article_model.dart';
import 'package:ny_times/screens/article_info_screen.dart';

class MainScreen extends StatefulWidget {
  const MainScreen({Key? key}) : super(key: key);

  @override
  State<MainScreen> createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  final controller = MainController();
  final scrollController = ScrollController();
  List<Results> _articles = [];
  List<Results> _searchArticles = [];
  int commonIndex = 0;
  int tabIndex = 0;
  bool isSection = false;
  bool searched = false;
  final TextEditingController searchController = TextEditingController();

  _fetchArticles(int index) async {
    await controller.getArticles(index);
    final box = await Hive.openBox('articles');
    if (await InternetConnectionChecker().hasConnection) {
      setState(() {
        _articles = controller.articles;
      });
    } else {
      setState(() {
        _articles = box.get('results');
      });
    }
  }

  _fetchSectionArticle(int index) async {
    await controller.getArticles(index);
    setState(() {
      _articles = controller.newArticles;
    });
  }

  _searchArticle() {
    if (searchController.text.length >= 3) {
      setState(() {
        _searchArticles = _articles
                .where((item) => item.title!
                    .toLowerCase()
                    .contains(searchController.text.toLowerCase()))
                .toList() +
            _articles
                .where((item) => item.section!
                    .toLowerCase()
                    .contains(searchController.text.toLowerCase()))
                .toList();
      });
    } else {
      setState(() {
        searched = false;
      });
    }
  }

  @override
  void initState() {
    super.initState();
    _fetchArticles(commonIndex);
    scrollController.addListener(() {
      if (scrollController.position.maxScrollExtent ==
              scrollController.offset &&
          isSection == false) {
        _fetchArticles(commonIndex++);
      }
    });
  }

  @override
  void dispose() {
    scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (_articles.isEmpty) {
      _fetchArticles(commonIndex);
    }
    ;
    controller.sections.removeRange(1, controller.sections.length);
    controller.sections.addAll(controller.sectionArticles);
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0,
        toolbarHeight: 100,
        title: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: const [
            SizedBox(height: 28),
            Text(
              'Browse',
              style: TextStyle(color: Color(0xFF333647), fontSize: 24),
            ),
            SizedBox(height: 8),
            Text('Discover things of this world',
                style: TextStyle(color: Color(0xFF7C82A1), fontSize: 16)),
          ],
        ),
      ),
      body: Column(
        children: [
          const SizedBox(height: 24),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: TextFormField(
              controller: searchController,
              onEditingComplete: () {
                searched = true;
                _searchArticle();
              },
              cursorColor: const Color(0xFF7C82A1),
              decoration: const InputDecoration(
                prefixIcon:
                    Icon(Icons.search, color: Color(0xFF7C82A1), size: 26),
                hintText: 'Search',
                suffixIcon: Icon(Icons.mic_none_outlined,
                    color: Color(0xFF7C82A1), size: 26),
                filled: true,
                fillColor: Color(0xFFF3F4F6),
                enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Color(0xFFF3F4F6))),
                focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Color(0xFFF3F4F6))),
              ),
            ),
          ),
          const SizedBox(height: 24),
          SizedBox(
            height: 32,
            child: ListView.builder(
              scrollDirection: Axis.horizontal,
              itemCount: controller.sections.length,
              itemBuilder: (context, index) {
                return Container(
                  padding: const EdgeInsets.symmetric(horizontal: 16),
                  margin: const EdgeInsets.only(left: 20),
                  decoration: BoxDecoration(
                    color: index == tabIndex
                        ? const Color(0xFF475AD7)
                        : const Color(0xFFF3F4F6),
                    borderRadius: const BorderRadius.all(Radius.circular(16)),
                  ),
                  child: InkWell(
                      onTap: () {
                        if (index != 0) {
                          _fetchSectionArticle(index - 1);
                          isSection = true;
                        } else {
                          _fetchArticles(commonIndex);
                          isSection = false;
                        }
                        setState(() {
                          tabIndex = index;
                        });
                      },
                      child: Center(
                          child: Text(
                        controller.sections[index],
                        style: TextStyle(
                            color: index == tabIndex
                                ? Colors.white
                                : Colors.black),
                      ))),
                );
              },
            ),
          ),
          _articles.isNotEmpty
              ? Expanded(
                  child: ListView.builder(
                    controller: scrollController,
                    itemCount:
                        searched ? _searchArticles.length : _articles.length,
                    itemBuilder: (BuildContext context, int index) {
                      return InkWell(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => ArticleInfoScreen(
                                        index: index,
                                        controller: controller,
                                      )));
                        },
                        child: Container(
                          margin: const EdgeInsets.only(
                              left: 20, right: 20, top: 24),
                          height: 100,
                          padding: const EdgeInsets.symmetric(horizontal: 16),
                          alignment: Alignment.centerLeft,
                          decoration: const BoxDecoration(
                              color: Color(0xFFF3F4F6),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(16))),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                searched
                                    ? _searchArticles[index].title!
                                    : _articles[index].title!,
                                style: const TextStyle(
                                    color: Color(0xFF333647),
                                    fontWeight: FontWeight.w600),
                              ),
                              const SizedBox(height: 20),
                              Text(
                                searched
                                    ? _searchArticles[index].byline!
                                    : _articles[index].byline!,
                                style: const TextStyle(
                                    color: Color(0xFF7C82A1),
                                    fontWeight: FontWeight.w400),
                              ),
                            ],
                          ),
                        ),
                      );
                    },
                  ),
                )
              : const Center(child: CircularProgressIndicator()),
        ],
      ),
    );
  }
}
