import 'package:flutter/material.dart';
import 'package:ny_times/controllers/main_controller.dart';
import 'package:webview_flutter/webview_flutter.dart';

class ArticleInfoScreen extends StatefulWidget {
  final int index;
  final MainController controller;

  const ArticleInfoScreen({Key? key, required this.index, required this.controller}) : super(key: key);

  @override
  State<ArticleInfoScreen> createState() => _ArticleInfoScreenState();
}

class _ArticleInfoScreenState extends State<ArticleInfoScreen> {
  @override
  void initState() {
    super.initState();
    setState(() {
      widget.controller.articles;
    });
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: WebViewWidget(
          controller: WebViewController()
            ..setJavaScriptMode(JavaScriptMode.unrestricted)
            ..loadRequest(Uri.parse(widget.controller.articles[widget.index].url!)),
        ),
      ),
    );
  }
}
