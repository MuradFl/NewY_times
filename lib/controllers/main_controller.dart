import 'package:hive/hive.dart';
import 'package:ny_times/models/article_model.dart';
import 'package:ny_times/repositories/home_repository.dart';

class MainController {
  final HomeRepository _homeRepository = HomeRepository();
  List<Results> _articles = [];
  List<Results> _newArticles = [];
  final List<String> _sectionArticles = [
    "Arts",
    "Automobiles",
    "Books",
    "Business",
    "Fashion",
    "Food",
    "Health",
    "Home",
    "Insider",
    "Magazine",
    "Movies",
    "Nyregion",
    "Obituaries",
    "Opinion",
    "Politics",
    "Realestate",
    "Science",
    "Sports",
    "Sundayreview",
    "Technology",
    "Theater",
    "T - magazine",
    "Travel",
    "Upshot",
    "Us",
    "World"
  ];

  final List _sections = ["Random"];

  List get sections => _sections;

  List<Results> get articles => _articles;

  List<Results> get newArticles => _newArticles;

  List<String> get sectionArticles => _sectionArticles;

  Future getArticles(int index) async {
    final box = await Hive.openBox('articles');
    final response = await _homeRepository.fetch(_sectionArticles[index]);
    _newArticles = response;
    _newArticles.shuffle();
    _articles += _newArticles;
    if (_articles.length >= 40) {
      box.put('results', _articles.sublist(_articles.length - 40));
    } else {
      box.put('results', _articles);
    }

    return response;
  }
}
